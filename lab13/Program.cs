﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Лабораторная_работа_13
{
	class Program
	{
		public struct Turfurm
		{
			public string Turname;
			public string Address;
			public double YearTurn;
			public LinkedList<int> chain1;
		};
		public struct Country
		{
			public string Name;
            public double Population;
            public string Valuta;
			public LinkedList<int> chain2;
		};
        public static void Menu()                   
		{
			Console.WriteLine("<<Меню пользователя>>");
			Console.WriteLine("---------------------------------------");
			Console.WriteLine("1-Ввод нового элемента в список турфирм");
			Console.WriteLine("2-Ввод нового элемента в список стран");
			Console.WriteLine("3-вывести списки на экран");
			Console.WriteLine("4-Связать элементы двух списков");
			Console.WriteLine("5-Сохранить списки в бинарный файл");
			Console.WriteLine("6-Загрузить из бинарного файла");
			Console.WriteLine("7-выход из программы");
			Console.WriteLine("---------------------------------------");
			Console.WriteLine();
			Console.WriteLine("Выберите пункт");
		}

        public static void ShowTurfirm(LinkedList<Turfurm> Turfirm_new)
		{
			int i = 1;
			LinkedListNode<Turfurm> node1;
			Console.WriteLine("<<Элементы списка турфирм>>");
			Console.WriteLine("-----------------------------------------------------------------------");
			for (node1 = Turfirm_new.First; node1 != null; node1 = node1.Next)
			{
				Console.WriteLine(i + ") " + node1.Value.Turname + ", " + node1.Value.Address + ", годовой оборот - " + node1.Value.YearTurn);
				i++;
			}
			Console.WriteLine("-----------------------------------------------------------------------");
		}
        public static void ShowCountry(LinkedList<Country> Country_new)
		{
			int i = 1;
			LinkedListNode<Country> node2;
			Console.WriteLine("<<Элементы списка стран>>");
			Console.WriteLine("-----------------------------------------------------------------------");
			for (node2 = Country_new.First; node2 != null; node2 = node2.Next)
			{
				Console.WriteLine(i + ") " + node2.Value.Name + ", " + " население " + node2.Value.Population + " нац. валюта " + node2.Value.Valuta);
				i++;
			}
			Console.WriteLine("-----------------------------------------------------------------------");
		}


        public static LinkedList<Turfurm> EnterTur(ref LinkedList<Turfurm> Turfirm_new)
		{
			Turfurm myElem;

			Console.Clear();
			Console.WriteLine("Название турфирмы:");
			myElem.Turname = Console.ReadLine();
			Console.Clear();
			myElem.Address = Console.ReadLine();
            Console.Clear();
			myElem.chain1 = new LinkedList<int>();
			Console.WriteLine("Годовой оборот:");
			myElem.YearTurn = Convert.ToDouble(Console.ReadLine());
			Console.Clear();
			while (myElem.YearTurn <= 0)
			{
				Console.WriteLine("Недопустимое значение!");
				myElem.YearTurn = Convert.ToDouble(Console.ReadLine());
				Console.Clear();
			}
			Turfirm_new.AddLast(myElem);
			return Turfirm_new;
		}
        public static LinkedList<Country> EnterCountry(ref LinkedList<Country> Turfirm_new)
		{
			Country myElem;
			Console.Clear();
			Console.WriteLine("Страна:");
			myElem.Name = Console.ReadLine();
			Console.Clear();
			Console.WriteLine("Популяция:");
			myElem.Population = Convert.ToDouble(Console.ReadLine());
			Console.Clear();
			while (myElem.Population <= 0)
			{
				Console.WriteLine("Недопустимое значение!");
				myElem.Population = Convert.ToDouble(Console.ReadLine());
				Console.Clear();
			}
			Console.WriteLine("Национальная валюта:");
            myElem.Valuta = Console.ReadLine();
			Console.Clear();
			myElem.chain2 = new LinkedList<int>();
			Turfirm_new.AddLast(myElem);
			return Turfirm_new;
		}


        public static void ShowList(LinkedList<Turfurm> Turfirm_new1, LinkedList<Country> Turfirm_new2, int Max1, int Max2)
		{
			int i = 1;
			string index1;
			int index2;
			LinkedListNode<Turfurm> node1;
			LinkedListNode<Country> node2;
			LinkedListNode<int> chain;

			Console.WriteLine("Какой список отобразить?");
			Console.WriteLine("1)Список турфирм");
			Console.WriteLine("2)Список стран");
			Console.WriteLine("3)Список Всех турфирм, связанных с опредленной страной");
			Console.WriteLine("4)Список Всех стран, связанных с опредленной турфирмой");
			index1 = Console.ReadLine();
			Console.Clear();
			while (index1 != "1" && index1 != "2" && index1 != "3" && index1 != "4")
			{
				Console.WriteLine("Введено неверное значение!");
				index1 = Console.ReadLine();
				Console.Clear();
			}
			index2 = Convert.ToInt32(index1);
			switch (index2)
			{
				case 1:
					{
						Console.WriteLine("Выполнить сортировку?");
						Console.WriteLine("1) Да");
						Console.WriteLine("2) Нет");
						index1 = Console.ReadLine();
						Console.Clear();
						while (index1 != "1" && index1 != "2")
						{
							Console.WriteLine("Введено неверное значение!");
							index1 = Console.ReadLine();
							Console.Clear();
						}
						index2 = Convert.ToInt32(index1);
						if (index2 == 1)
						{
							Console.WriteLine("<<Выберете способ сортировки>>");
							Console.WriteLine("1)По названию");
							Console.WriteLine("2)По адресу");
							Console.WriteLine("3)По годовому обороту");
							index1 = Console.ReadLine();
							Console.Clear();
							while (index1 != "1" && index1 != "2" && index1 != "3")
							{
								Console.WriteLine("Введено неверное значение!");
								index1 = Console.ReadLine();
								Console.Clear();
							}
							index2 = Convert.ToInt32(index1);
							switch (index2)
							{
								case 1:
									{
										for (int k = 0; k < Max1; k++)
										{
											for (node1 = Turfirm_new1.First; node1 != null; node1 = node1.Next)
											{
												if (node1.Next != null)
												{
													if (String.Compare(node1.Value.Turname, node1.Next.Value.Turname) > 0)
													{
														Turfirm_new1.AddAfter(node1.Next, node1.Value);
														Turfirm_new1.Remove(node1);
													}
												}
											}
										}
										ShowTurfirm(Turfirm_new1);
										break;
									}
								case 2:
									{
										for (int k = 0; k < Max1; k++)
										{
											for (node1 = Turfirm_new1.First; node1 != null; node1 = node1.Next)
											{
												if (node1.Next != null)
												{
													if (String.Compare(node1.Value.Address, node1.Next.Value.Address) > 0)
													{
														Turfirm_new1.AddAfter(node1.Next, node1.Value);
														Turfirm_new1.Remove(node1);
													}
												}
											}
										}
										ShowTurfirm(Turfirm_new1);
										break;
									}
								case 3:
									{
										for (int k = 0; k < Max1; k++)
										{
											for (node1 = Turfirm_new1.First; node1 != null; node1 = node1.Next)
											{
												if (node1.Next != null)
												{
													if (node1.Value.YearTurn > node1.Next.Value.YearTurn)
													{
														Turfirm_new1.AddAfter(node1.Next, node1.Value);
														Turfirm_new1.Remove(node1);
													}
												}
											}
										}
										ShowTurfirm(Turfirm_new1);
										break;
									}
							}
						}
						else
						{
							ShowTurfirm(Turfirm_new1);
						}
						break;
					}



				case 2:
					{
						Console.WriteLine("Выполнить сортировку?");
						Console.WriteLine("1) Да");
						Console.WriteLine("2) Нет");
						index1 = Console.ReadLine();
						Console.Clear();
						while (index1 != "1" && index1 != "2")
						{
							Console.WriteLine("Введено неверное значение!");
							index1 = Console.ReadLine();
							Console.Clear();
						}
						index2 = Convert.ToInt32(index1);
						if (index2 == 1)
						{
							Console.WriteLine("<<Выберете способ сортировки>>");
							Console.WriteLine("1)По названию");
							Console.WriteLine("2)По населению");
							Console.WriteLine("3)По валюте");
							index1 = Console.ReadLine();
							Console.Clear();
							while (index1 != "1" && index1 != "2" && index1 != "3")
							{
								Console.WriteLine("Введено неверное значение!");
								index1 = Console.ReadLine();
								Console.Clear();
							}
							index2 = Convert.ToInt32(index1);
							switch (index2)
							{
								case 1:
									{
										for (int k = 0; k < Max2; k++)
										{
											for (node2 = Turfirm_new2.First; node2 != null; node2 = node2.Next)
											{
												if (node2.Next != null)
												{
													if (String.Compare(node2.Value.Name, node2.Next.Value.Name) > 0)
													{
														Turfirm_new2.AddAfter(node2.Next, node2.Value);
														Turfirm_new2.Remove(node2);
													}
												}
											}
										}
										ShowCountry(Turfirm_new2);
										break;
									}
								case 2:
									{
										for (int k = 0; k < Max2; k++)
										{
											for (node2 = Turfirm_new2.First; node2 != null; node2 = node2.Next)
											{
												if (node2.Next != null)
												{
													if (node2.Value.Population > node2.Next.Value.Population)
													{
														Turfirm_new2.AddAfter(node2.Next, node2.Value);
														Turfirm_new2.Remove(node2);
													}
												}
											}
										}
										ShowCountry(Turfirm_new2);
										break;
									}
								case 3:
									{
										for (int k = 0; k < Max2; k++)
										{
											for (node2 = Turfirm_new2.First; node2 != null; node2 = node2.Next)
											{
												if (node2.Next != null)
												{
                                                    if (String.Compare(node2.Value.Valuta, node2.Next.Value.Valuta) > 0)
													{
														Turfirm_new2.AddAfter(node2.Next, node2.Value);
														Turfirm_new2.Remove(node2);
													}
												}
											}
										}
										ShowCountry(Turfirm_new2);
										break;
									}
							}
						}
						else
						{
							ShowCountry(Turfirm_new2);
						}
						break;
					}
				case 3:
					{
						{
							Console.WriteLine("Выберите номер страны");
							ShowCountry(Turfirm_new2);
							i = Convert.ToInt32(Console.ReadLine());
							Console.Clear();
							node2 = Turfirm_new2.First;
							for (int k = 1; k < i; k++)
							{
								node2 = node2.Next;
							}
							chain = node2.Value.chain2.First;
							Console.WriteLine("Связанные с выбранной страной турфирмы");
							Console.WriteLine("-----------------------------------------------------------------------");
							int z = 1;
							while (chain != null)
							{
								i = chain.Value;
								node1 = Turfirm_new1.First;
								for (int k = 1; k < i; k++)
								{
									node1 = node1.Next;
								}

								Console.WriteLine(z + ") " + node1.Value.Turname);
								Console.WriteLine("Адрес: " + node1.Value.Address);
                                Console.WriteLine("Годовой оборот: " + node1.Value.YearTurn);
								chain = chain.Next;
								z++;
							}
							Console.WriteLine("-----------------------------------------------------------------------");
							break;
						}
					}
				case 4:
					{
						Console.WriteLine("Выберите номер турфирмы");
						ShowTurfirm(Turfirm_new1);
						i = Convert.ToInt32(Console.ReadLine());
						Console.Clear();
						node1 = Turfirm_new1.First;
						for (int k = 1; k < i; k++)
						{
							node1 = node1.Next;
						}
						chain = node1.Value.chain1.First;
						Console.WriteLine("Связанные с выбранной турфирмой страны");
						Console.WriteLine("-----------------------------------------------------------------------");
						int z = 1;
						while (chain != null)
						{
							i = chain.Value;
							node2 = Turfirm_new2.First;
							for (int k = 1; k < i; k++)
							{
								node2 = node2.Next;
							}
							Console.WriteLine(z + ") " + node2.Value.Name + ", " + " население " + node2.Value.Population + " нац. валюта " + node2.Value.Valuta);
							chain = chain.Next;
							z++;
						}
						Console.WriteLine("-----------------------------------------------------------------------");
						break;
					}
			}
		}
        public static void Connections(ref LinkedList<Turfurm> Turfirm_new1, ref LinkedList<Country> Turfirm_new2)
		{
			string index1;
			int index2;
			int bones = 0;
			int Number1;
			int Number2;
			int block = 0;
			string block1 = "";
			LinkedListNode<Turfurm> node1;
			LinkedListNode<Country> node2;
			LinkedListNode<int> chain;
			Console.WriteLine("Выберите тип связи");
			Console.WriteLine("1)Связь турфирмы со страной");
			Console.WriteLine("2)Связь страны с турфирмой");
			index1 = Console.ReadLine();
			Console.Clear();
			while (index1 != "1" && index1 != "2")
			{
				Console.WriteLine("Введено неверное значение!");
				index1 = Console.ReadLine();
				Console.Clear();
			}
			index2 = Convert.ToInt32(index1);
			switch (index2)
			{
				case 1:
					{
						while (block == 0)
						{
							Console.WriteLine("Выберите номер турфирмы");
							ShowTurfirm(Turfirm_new1);
							Number1 = Convert.ToInt32(Console.ReadLine());
							Console.Clear();
							Console.WriteLine("Выберите номер страны, с которым необходимо связать турфирму");
							ShowCountry(Turfirm_new2);
							Number2 = Convert.ToInt32(Console.ReadLine());
							Console.Clear();
							node1 = Turfirm_new1.First;
							for (int k = 1; k < Number1; k++)
							{
								node1 = node1.Next;
							}
							for (chain = node1.Value.chain1.First; chain != null; chain = chain.Next)
							{
								if (Number2 == chain.Value)
								{
									Console.WriteLine("Данная страна уже связана с турфирмой!");
									Console.ReadKey(true);
									Console.Clear();
									bones = 1;
									break;
								}
								bones = 0;
							}
							if (bones == 0)
							{
								node1.Value.chain1.AddLast(Number2);
								if (node1.Next == null && node1.Previous == null)
								{
									Turfirm_new1.Remove(node1);
									Turfirm_new1.AddLast(node1);
								}
								if (node1.Next != null)
								{
									Turfirm_new1.AddAfter(node1.Next, node1.Value);
									Turfirm_new1.Remove(node1);
									node1 = Turfirm_new1.First;
									for (int k = 1; k < Number1; k++)
									{
										node1 = node1.Next;
									}
									Turfirm_new1.AddAfter(node1.Next, node1.Value);
									Turfirm_new1.Remove(node1);
								}
								if (node1.Next == null && node1.Previous != null)
								{
									Turfirm_new1.AddBefore(node1.Previous, node1.Value);
									Turfirm_new1.Remove(node1);
									node1 = Turfirm_new1.First;
									for (int k = 1; k < Number1; k++)
									{
										node1 = node1.Next;
									}
									Turfirm_new1.AddBefore(node1.Previous, node1.Value);
									Turfirm_new1.Remove(node1);
								}
							}
							Console.WriteLine("Продолжить связывать элементы?");
							Console.WriteLine("1)Да");
							Console.WriteLine("2)Нет");
							block1 = Console.ReadLine();
							while (block1 != "1" && block1 != "2")
							{
								Console.WriteLine("Введено неверное значение!");
								block1 = Console.ReadLine();
								Console.Clear();
							}
							if (block1 == "2")
							{
								block = 1;
							}
							Console.Clear();

						}
						break;
					}
				case 2:
					{
						while (block == 0)
						{
							Console.WriteLine("Выберите номер страны");
							ShowCountry(Turfirm_new2);
							Number1 = Convert.ToInt32(Console.ReadLine());
							Console.Clear();
							Console.WriteLine("Выберите номер турфирмы, с которой необходимо связать страну");
							ShowTurfirm(Turfirm_new1);
							Number2 = Convert.ToInt32(Console.ReadLine());
							Console.Clear();
							node2 = Turfirm_new2.First;
							for (int k = 1; k < Number1; k++)
							{
								node2 = node2.Next;
							}
							for (chain = node2.Value.chain2.First; chain != null; chain = chain.Next)
							{
								if (Number2 == chain.Value)
								{
									Console.WriteLine("Данная турфирма уже связана со страной!");
									Console.ReadKey(true);
									Console.Clear();
									bones = 1;
									break;
								}
								bones = 0;
							}
							if (bones == 0)
							{
								node2.Value.chain2.AddLast(Number2);
								if (node2.Next == null && node2.Previous == null)
								{
									Turfirm_new2.Remove(node2);
									Turfirm_new2.AddLast(node2);
								}
								if (node2.Next != null)
								{
									Turfirm_new2.AddAfter(node2.Next, node2.Value);
									Turfirm_new2.Remove(node2);
									node2 = Turfirm_new2.First;
									for (int k = 1; k < Number1; k++)
									{
										node2 = node2.Next;
									}
									Turfirm_new2.AddAfter(node2.Next, node2.Value);
									Turfirm_new2.Remove(node2);
								}
								if (node2.Next == null && node2.Previous != null)
								{
									Turfirm_new2.AddBefore(node2.Previous, node2.Value);
									Turfirm_new2.Remove(node2);
									node2 = Turfirm_new2.First;
									for (int k = 1; k < Number1; k++)
									{
										node2 = node2.Next;
									}
									Turfirm_new2.AddBefore(node2.Previous, node2.Value);
									Turfirm_new2.Remove(node2);
								}
							}
							Console.WriteLine("Продолжить связывание?");
							Console.WriteLine("1)Да");
							Console.WriteLine("2)Нет");
							block1 = Console.ReadLine();
							while (block1 != "1" && block1 != "2")
							{
								Console.WriteLine("Введено неверное значение!");
								block1 = Console.ReadLine();
								Console.Clear();
							}
							if (block1 == "2")
							{
								block = 1;
							}
							Console.Clear();
						}
						break;
					}
			}
		}


        public static void BinarySave(LinkedList<Turfurm> Turfirm_new1, LinkedList<Country> Turfirm_new2, ref string way)
		{
			LinkedListNode<Turfurm> node1;
			LinkedListNode<Country> node2;
			string Choise1;
			int AllChain = 0;
			Console.WriteLine("Введите путь к файлу! (формат бинарного файла dat");
			way = Console.ReadLine();
			FileInfo file = new FileInfo(way);
			while (file.Exists == false)
			{
				Console.WriteLine("Файла с таким путем не существует, выбрать другой файл или создать по данному адресу новый?");
				Console.WriteLine("1)Выбрать существующий путь");
				Console.WriteLine("2)Создать новый файл по данному пути");
				Choise1 = Convert.ToString(Console.ReadLine());
				while ((Choise1 != "1") && (Choise1 != "2"))
				{
					Console.Clear();
					Console.WriteLine("Неверный выбор!");
					Choise1 = Convert.ToString(Console.ReadLine());
				}
				if (Choise1 == "1")
				{

					Console.Clear();
					Console.WriteLine("Другой путь:");
					way = Console.ReadLine();
					file = new FileInfo(way);
					Console.Clear();

				}
				if (Choise1 == "2")
				{
					break;
				}
				Console.Clear();
			}

			BinaryWriter writer = new BinaryWriter(File.Open(way, FileMode.OpenOrCreate));
			writer.Close();
			writer = new BinaryWriter(File.Open(way, FileMode.Truncate));
			foreach (Turfurm s in Turfirm_new1)
			{
				writer.Write(s.Turname);
				writer.Write(s.Address);
				writer.Write(s.YearTurn);
				foreach (int s1 in s.chain1)
				{
					AllChain++;
				}
				writer.Write(AllChain);
				foreach (int s1 in s.chain1)
				{
					node2 = Turfirm_new2.First;
					for (int k = 1; k < s1; k++)
					{
						node2 = node2.Next;
					}
					writer.Write(node2.Value.Name);
				}
				AllChain = 0;
			}
			foreach (Country k in Turfirm_new2)
			{
				writer.Write(k.Name);
				writer.Write(k.Population);
				writer.Write(k.Valuta);
				foreach (int k1 in k.chain2)
				{
					AllChain++;
				}
				writer.Write(AllChain);
				foreach (int k1 in k.chain2)
				{
					node1 = Turfirm_new1.First;
					for (int r = 1; r < k1; r++)
					{
						node1 = node1.Next;
					}
					writer.Write(node1.Value.Turname);
				}
				AllChain = 0;
			}
			writer.Close();
			Console.WriteLine("Данные записаны в бинарный файл! ");
			Console.WriteLine("Нажмите любую клавишу, чтобы вернуться в меню");
		}
        public static void BinaryLoad(LinkedList<Turfurm> Turfirm_new1, LinkedList<Country> Turfirm_new2, string way, int Max1, int Max2)
		{
			BinaryReader reader = new BinaryReader(File.Open(way, FileMode.Open));
			Console.WriteLine("<<Элементы списка турфирм>>");
			Console.WriteLine("-----------------------------------------------------------------------");
			int j = 1;
			for (int k = 0; k < Max1; k++)
			{
                string Turname = reader.ReadString();
                string Address = reader.ReadString();
                double YearTurn = reader.ReadDouble();
				int AllChain = reader.ReadInt32();
				Console.WriteLine(j + ") " + Turname + ", " + Address + ", годовой оборот - " + YearTurn);
				j++;
				Console.WriteLine("Связанные с турфирмой страны:");
				for (int k1 = 0; k1 < AllChain; k1++)
				{
					string Name = reader.ReadString();
					Console.WriteLine(Name);
				}
				Console.WriteLine("");
			}

			Console.WriteLine("-----------------------------------------------------------------------");
			Console.WriteLine("<<Элементы списка стран>>");
			Console.WriteLine("-----------------------------------------------------------------------");
			int i = 1;
			for (int k = 0; k < Max2; k++)
			{

				string Name = reader.ReadString();
                double Population = reader.ReadDouble();
                string Valuta = reader.ReadString();
				int AllChain = reader.ReadInt32();
				Console.WriteLine(i + ") " + Name + ", " + "население " + Population + " нац. валюта " + Valuta);
				i++;
				Console.WriteLine("Связанные со страной турфирмы:");
				for (int k1 = 0; k1 < AllChain; k1++)
				{
					string Turname = reader.ReadString();
                    Console.WriteLine(Turname);
				}
				Console.WriteLine("");
			}

			Console.WriteLine("-----------------------------------------------------------------------");
			reader.Close();
			Console.WriteLine("Нажмите любую клавишу, чтобы вернуться в меню");
		}
		static void Main(string[] args)
		{
            LinkedList<Turfurm> Turfr = new LinkedList<Turfurm>();
            LinkedList<Country> State = new LinkedList<Country>();
			int maxsize1 = 0, maxsize2 = 0;
			int blockBinary = 0;
			string WayBin = null;
			int a = 0;
			while (a == 0)
			{
				Menu();
				a = Convert.ToInt32(Console.ReadLine());
				Console.Clear();
				if (a < 1 || a > 7)
				{
					Console.WriteLine("Введено неверное значение");
					a = Convert.ToInt32(Console.ReadLine());
					Console.Clear();
				}
				switch (a)
				{
					case 1:
						{
							Console.Clear();
							maxsize1++;
							EnterTur(ref Turfr);
							Console.WriteLine("В список добавлен новый элемент. Нажмите любую клавишу, чтобы вернуться в меню! ");
							Console.ReadKey(true);
							Console.Clear();
							a = 0;
							break;
						}
					case 2:
						{
							Console.Clear();
							maxsize2++;
							EnterCountry(ref State);
							Console.WriteLine("В список добавлен новый элемент. Нажмите любую клавишу, чтобы вернуться в меню! ");
							Console.ReadKey(true);
							Console.Clear();
							a = 0;
							break;
						}
					case 3:
						{
							if (maxsize1 == 0 & maxsize2 == 0)
							{
								Console.WriteLine("Списки не заполнены, прежде введите данные!");
								a = 0;
								break;
							}
							ShowList(Turfr, State, maxsize1, maxsize2);
							Console.ReadKey(true);
							Console.Clear();
							a = 0;
							break;
						}
					case 4:
						{
							if (maxsize1 == 0 || maxsize2 == 0)
							{
								Console.WriteLine("Один из двух или оба списка не заполнены, прежде введите данные!");
								a = 0;
								break;
							}
							Connections(ref Turfr, ref State);
							a = 0;
							break;
						}
					case 5:
						{
							if (maxsize1 == 0 && maxsize2 == 0)
							{
								Console.WriteLine("В списках нет элементов. Нечего записать в бинарный файл!");
								a = 0;
								break;
							}
							BinarySave(Turfr, State, ref WayBin);
							Console.ReadKey(true);
							Console.Clear();
							blockBinary = 1;
							a = 0;
							break;
						}
					case 6:
						{
							if (blockBinary == 0)
							{
								Console.WriteLine("Прежде сохраните данные в бинарный файл!");
								a = 0;
								break;
							}
							BinaryLoad(Turfr, State, WayBin, maxsize1, maxsize2);
							Console.ReadKey(true);
							Console.Clear();
							a = 0;
							break;
						}
				}
			}
		}
	}
}
